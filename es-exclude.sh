#!/usr/bin/env bash

# this script used to exclude a logstash elasticsearch server from cluster, when you need do the maintenance.
# refer: http://www.elasticsearch.org/guide/en/elasticsearch/reference/current/modules-cluster.html

# name sample:  "name" : "node4", 
# http://localhost:9200/_nodes/process?pretty
# the command will be es-exclude.sh node4 

if [ "x$1" == "x" ];then
  echo "Usage: $0 Name [Server]"
  exit 1
fi

# $2(Server) is elasticsearch server name or load balancer name, it is option. If $2 is not assigned, localhost is default hostname.
if [ "x$2" == "x" ];then
  server="localhost"
else
  server=$2
fi

curl -XPUT $server:9200/_cluster/settings -d '{ "transient" :{ "cluster.routing.allocation.exclude.name" : "'$1'"}}';echo
