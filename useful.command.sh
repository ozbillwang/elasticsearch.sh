# Useful command collection
# all command start from curl -XGET http://localhost:9200
# for example, the first command will be combined and run as:
# curl -XGET http://localhost:9200/_cluster/health?pretty
# replace localhost to real elasticsearch server or load balancer if not running on localhost.

# The cluster health API 
_cluster/health?pretty

# Node status.
_nodes/process?pretty

_cat/shards
_cat/nodes
